# Demo Aplica��o Teste Builders API REST CRUD

### Requisitos

 - JDK 11
 - Docker (PostgreSQL)
 - Gradle
 
### Docker

Arquivo de configura��o do docker para levantar o banco de dados PostgreSQL est� em `demo/docker/docker-compose.yml`

Para rodar:

    cd demo/docker
    docker-compose up -d

O docker est� rodando em uma m�quina virtual com ip: `192.168.99.100`, caso seu docker rode no host `localhost` modifique a propriedade `spring.datasource.url` no arquivo `application.properties`:

De:

    spring.datasource.url= jdbc:postgresql://192.168.99.100:5432/demo
    
Para:

    spring.datasource.url= jdbc:postgresql://localhost:5432/demo





