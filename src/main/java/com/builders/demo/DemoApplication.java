package com.builders.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.builders.demo.web.hateoas.ClienteModelAssembler;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public ClienteModelAssembler assembler() {
		return new ClienteModelAssembler();
	}
}
