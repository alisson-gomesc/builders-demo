package com.builders.demo.modelo;

import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente {
	
	@Id @GeneratedValue
	private Long id;
	@NotBlank(message = "Nome � obrigat�rio")
	private String nome;
	@NotBlank(message = "CPF � obrigat�rio")
	private String cpf;
	@NotNull(message = "Data de nascimento � obrigat�rio")
	private LocalDate dataNascimento;
	
	public Cliente() {
		super();
	}
	
	public Cliente(String nome, String cpf, LocalDate dataNascimento) {
		this(null, nome, cpf, dataNascimento);
	}

	public Cliente(Long id, String nome, String cpf, LocalDate dataNascimento) {
		this();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}

	public int getIdade() {
		if (this.dataNascimento == null)
			return 0;
		
		return Period.between(this.dataNascimento, LocalDate.now()).getYears();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nome=" + nome + ", cpf=" + cpf + ", dataNascimento=" + dataNascimento
				+ ", idade=" + getIdade() + "]";
	}
	
	
}
