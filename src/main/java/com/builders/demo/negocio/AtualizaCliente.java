package com.builders.demo.negocio;

import com.builders.demo.modelo.Cliente;

public interface AtualizaCliente {

	Cliente executar(Cliente clienteModificado);

}