package com.builders.demo.negocio;

import com.builders.demo.modelo.Cliente;

public interface AtualizaCpfCliente {

	Cliente executar(Cliente clienteModificado);

}