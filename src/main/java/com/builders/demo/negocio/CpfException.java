package com.builders.demo.negocio;

public class CpfException extends RuntimeException {

	private static final long serialVersionUID = -3568317594940820825L;

	public CpfException(String mensagem) {
		super(mensagem);
	}

}
