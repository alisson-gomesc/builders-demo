package com.builders.demo.negocio;

import com.builders.demo.modelo.Cliente;

public interface ExcluiCliente {

	/**
	 * Apaga o registro usando o identificador do {@code cliente}
	 * @param cliente com o identificador 
	 */
	void executar(Cliente cliente);

	/**
	 * Apaga o registro do cliente usando somente o identificador
	 * @param id identificador do cliente
	 */
	void executar(Long id);

}