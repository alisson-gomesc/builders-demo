package com.builders.demo.negocio;

import com.builders.demo.modelo.Cliente;

public interface InsereCliente {

	Cliente executar(Cliente clienteNovo);

}