package com.builders.demo.negocio;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.builders.demo.modelo.Cliente;

public interface ProcuraCliente {

	/**
	 * Busca o cliente usando a chave prim�ria.
	 * 
	 * @param id chave prim�ria
	 * @return retorna o cliente encontrado ou {@code Optional#empty()} se nada for encontrado.
	 */
	Optional<Cliente> comId(Long id);

	/**
	 * Lista todos os clientes.
	 * 
	 * @param pageable parametros para pagina��o
	 * @return retorna todos os clientes do banco de dados ou uma lista vazia se nada for encontrado.
	 */
	Page<Cliente> todos(Pageable pageable);

	/**
	 * Busca o cliente pelo nome.
	 * 
	 * @param nome do cliente
	 * @return retorna o cliente encontrado ou {@code Optional#empty()} se nada for encontrado.
	 */
	Optional<Cliente> comNome(String nome);

	/**
	 * Busca o cliente pelo nome.
	 * 
	 * @param nome do cliente
	 * @param cpf do cliente
	 * @param pageable parametros para pagina��o
	 * @return retorna os clientes que contenham parte do nome ou CPF
	 */
	Page<Cliente> comNomeOuCpf(String nome, String cpf, Pageable pageable);

}