package com.builders.demo.negocio;

public class RegistroNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = -4148254237486814062L;

	public RegistroNaoEncontradoException(Long id) {
		super("N�o encontrou o registro " + id);
	}

}
