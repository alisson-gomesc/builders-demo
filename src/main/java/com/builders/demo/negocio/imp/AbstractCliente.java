package com.builders.demo.negocio.imp;

import com.builders.demo.repositorio.RepositorioCliente;

public abstract class AbstractCliente {

	private RepositorioCliente repositorio;

	
	public AbstractCliente(RepositorioCliente repositorio) {
		this.repositorio = repositorio;
	}

	protected RepositorioCliente getRepositorio() {
		return repositorio;
	}
}
