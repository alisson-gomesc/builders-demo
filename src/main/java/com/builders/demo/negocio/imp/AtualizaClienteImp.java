package com.builders.demo.negocio.imp;

import org.springframework.stereotype.Service;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.AtualizaCliente;
import com.builders.demo.negocio.CpfException;
import com.builders.demo.repositorio.RepositorioCliente;
import com.builders.demo.validadores.ValidadorUtil;

/**
 * Atualiza o registro do cliente no banco de dados.
 * 
 * @author Alisson Gomes
 */
@Service
public class AtualizaClienteImp extends AbstractCliente implements AtualizaCliente {

	public AtualizaClienteImp(RepositorioCliente repositorio) {
		super(repositorio);
	}

	@Override
	public Cliente executar(Cliente clienteModificado) {

		if(ValidadorUtil.getCpf(clienteModificado.getCpf()).isInvalido())
			throw new CpfException("CPF inv�lido: " + clienteModificado.getCpf());

		Cliente clienteSalvo = 
		getRepositorio().findById(clienteModificado.getId()).map(cliente -> {
			cliente.setNome(clienteModificado.getNome());
			cliente.setCpf(clienteModificado.getCpf());
			cliente.setDataNascimento(clienteModificado.getDataNascimento());
			return getRepositorio().save(cliente);
		}).orElseGet(() -> {
			return getRepositorio().save(clienteModificado);
		});

		return clienteSalvo;
	}
}
