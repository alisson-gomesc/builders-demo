package com.builders.demo.negocio.imp;

import org.springframework.stereotype.Service;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.AtualizaCpfCliente;
import com.builders.demo.negocio.CpfException;
import com.builders.demo.negocio.RegistroNaoEncontradoException;
import com.builders.demo.repositorio.RepositorioCliente;

/**
 * Atualiza o CPF do cliente para aqueles com menos de 18 anos.
 * 
 * @author Alisson Gomes
 */
@Service
public class AtualizaCpfClienteImp extends AbstractCliente implements AtualizaCpfCliente {

	public AtualizaCpfClienteImp(RepositorioCliente repositorio) {
		super(repositorio);
	}

	@Override
	public Cliente executar(Cliente clienteModificado) {

		if(clienteModificado.getIdade() > 17)
			throw new CpfException("CPF com maioridade n�o pode ser alterado");

		Cliente clienteSalvo = getRepositorio().findById(clienteModificado.getId()).map(cliente -> {
			cliente.setCpf(clienteModificado.getCpf());
			return getRepositorio().save(cliente);
		}).orElseThrow(() -> new RegistroNaoEncontradoException(clienteModificado.getId()));

		return clienteSalvo;
	}
}
