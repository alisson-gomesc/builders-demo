package com.builders.demo.negocio.imp;

import com.builders.demo.negocio.AtualizaCliente;
import com.builders.demo.negocio.AtualizaCpfCliente;
import com.builders.demo.negocio.ExcluiCliente;
import com.builders.demo.negocio.InsereCliente;
import com.builders.demo.negocio.ProcuraCliente;

/**
 * Interface que funciona como uma fachada para todas as opera��es de um cliente.
 * 
 * @author Alisson Gomes
 *
 */
public interface CrudCliente {

	InsereCliente inserir();

	ExcluiCliente excluir();

	AtualizaCliente atualizar();

	AtualizaCpfCliente atualizarCpf();

	ProcuraCliente procura();

}