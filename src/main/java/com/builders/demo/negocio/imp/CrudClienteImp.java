package com.builders.demo.negocio.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.builders.demo.negocio.AtualizaCliente;
import com.builders.demo.negocio.AtualizaCpfCliente;
import com.builders.demo.negocio.ExcluiCliente;
import com.builders.demo.negocio.InsereCliente;
import com.builders.demo.negocio.ProcuraCliente;

@Service
public class CrudClienteImp implements CrudCliente {

	@Autowired private InsereCliente insere;
	@Autowired private AtualizaCliente atualiza;
	@Autowired private AtualizaCpfCliente atualizaCpf;
	@Autowired private ExcluiCliente exclui;
	@Autowired private ProcuraCliente procura;

	@Override
	public InsereCliente inserir() {
		return insere;
	}
	@Override
	public ExcluiCliente excluir() {
		return exclui;
	}
	@Override
	public AtualizaCliente atualizar() {
		return atualiza;
	}
	@Override
	public AtualizaCpfCliente atualizarCpf() {
		return atualizaCpf;
	}
	@Override
	public ProcuraCliente procura() {
		return procura;
	}
}
