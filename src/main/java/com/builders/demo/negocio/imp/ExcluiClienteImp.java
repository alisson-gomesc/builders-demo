package com.builders.demo.negocio.imp;

import org.springframework.stereotype.Service;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.ExcluiCliente;
import com.builders.demo.repositorio.RepositorioCliente;

/**
 * Apaga o registro cliente do banco de dados.
 * 
 * @author Alisson Gomes
 */
@Service
public class ExcluiClienteImp extends AbstractCliente implements ExcluiCliente {


	public ExcluiClienteImp(RepositorioCliente repositorio) {
		super(repositorio);
	}

	/**
	 * Apaga o registro usando o identificador do {@code cliente}
	 * @param cliente com o identificador 
	 */
	@Override
	public void executar(Cliente cliente) {
		getRepositorio().delete(cliente);
	}

	/**
	 * Apaga o registro do cliente usando somente o identificador
	 * @param id identificador do cliente
	 */
	@Override
	public void executar(Long id) {
		getRepositorio().deleteById(id);
	}
}
