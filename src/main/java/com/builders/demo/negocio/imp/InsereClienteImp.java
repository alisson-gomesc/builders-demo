package com.builders.demo.negocio.imp; 

import org.springframework.stereotype.Service;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.CpfException;
import com.builders.demo.negocio.InsereCliente;
import com.builders.demo.repositorio.RepositorioCliente;
import com.builders.demo.validadores.ValidadorUtil;

/**
 * Salva o registro de cliente no banco de dados.
 * 
 * @author Alisson Gomes
 */
@Service
public class InsereClienteImp extends AbstractCliente implements InsereCliente {

	
	public InsereClienteImp(RepositorioCliente repositorio) {
		super(repositorio);
	}

	@Override
	public Cliente executar(Cliente clienteNovo) {
		
		if(ValidadorUtil.getCpf(clienteNovo.getCpf()).isInvalido())
			throw new CpfException("CPF inv�lido: " + clienteNovo.getCpf());

		return getRepositorio().save(clienteNovo);
	}
}