package com.builders.demo.negocio.imp;

import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.ProcuraCliente;
import com.builders.demo.repositorio.RepositorioCliente;

/**
 * Busca o(s) registro(s) de cliente(s) no banco de dados.
 * 
 * @author Alisson Gomes
 */
@Service
public class ProcuraClienteImp extends AbstractCliente implements ProcuraCliente {

	public ProcuraClienteImp(RepositorioCliente repositorio) {
		super(repositorio);
	}
	
	/**
	 * Busca o cliente usando a chave prim�ria.
	 * 
	 * @param id chave prim�ria
	 * @return retorna o cliente encontrado ou {@code Optional#empty()} se nada for encontrado.
	 */
	@Override
	public Optional<Cliente> comId(Long id) {
		return getRepositorio().findById(id);
	}

	/**
	 * Lista todos os clientes.
	 * 
	 * @param pageable parametros para pagina��o
	 * @return retorna todos os clientes do banco de dados ou uma lista vazia se nada for encontrado.
	 */
	@Override
	public Page<Cliente> todos(Pageable pageable) {
		return getRepositorio().findAll(pageable);
	}

	/**
	 * Busca o cliente pelo nome.
	 * 
	 * @param nome do cliente
	 * @return retorna o cliente encontrado ou {@code Optional#empty()} se nada for encontrado.
	 */
	@Override
	public Optional<Cliente> comNome(String nome) {
		Cliente clienteEncontrado = getRepositorio().findByNome(nome);
		Optional<Cliente> cliente = Optional.empty();
		if(Objects.nonNull(clienteEncontrado))
			cliente = Optional.of(clienteEncontrado);
		return cliente;
	}
	
	/**
	 * Busca o cliente pelo nome.
	 * 
	 * @param nome do cliente
	 * @param cpf do cliente
	 * @param pageable parametros para pagina��o
	 * @return retorna os clientes que contenham parte do nome ou CPF
	 */
	@Override
	public Page<Cliente> comNomeOuCpf(String nome, String cpf, Pageable pageable) {
		return getRepositorio().findByNomeLikeOrCpfLike("%"+nome+"%", "%"+cpf+"%", pageable);
	}

}
