/**
 * Este pacote mant�m as regras de neg�cio da aplica��o as quais devem ser implementadas seguindo o princ�pio 
 * <a href="https://en.wikipedia.org/wiki/Single-responsibility_principle">Single responsibility</a>.
 * 
 * As implementa��es devem ser <a href="https://pt.wikipedia.org/wiki/Plain_Old_Java_Objects">POJOs</a> com escopo Singleton
 * 
 * 
 * @author Alisson Gomes
 */
package com.builders.demo.negocio;