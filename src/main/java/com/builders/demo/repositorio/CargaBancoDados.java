package com.builders.demo.repositorio;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.builders.demo.modelo.Cliente;

/**
 * Classe utlizada apenas para carregar alguns dados de cliente quando rodar o projeto.
 *
 */
@Configuration
public class CargaBancoDados {

	private static final Logger log = LoggerFactory.getLogger(CargaBancoDados.class);

	  @Bean
	  CommandLineRunner carregarDadosBanco(RepositorioCliente repositorio) {

	    return args -> {
	      log.info("Pre carga " + repositorio.save(new Cliente("Joao",  "14785236985", LocalDate.of(2015, 10, 20))));
	      log.info("Pre carga " + repositorio.save(new Cliente("Maria",  "36985214789", LocalDate.of(2000, 9, 19))));
	      log.info("Pre carga " + repositorio.save(new Cliente("Silvio", "85274196325", LocalDate.of(1995, 8, 9))));
	      log.info("Pre carga " + repositorio.save(new Cliente("Ada",    "25469871236", LocalDate.of(2005, 11, 30))));
	      log.info("Pre carga " + repositorio.save(new Cliente("Eva",    "22587412369", LocalDate.of(2010, 10, 20))));
	    };
	  }
}
