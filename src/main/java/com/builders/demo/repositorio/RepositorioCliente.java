package com.builders.demo.repositorio;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.builders.demo.modelo.Cliente;

@Repository
public interface RepositorioCliente extends PagingAndSortingRepository<Cliente, Long> {

	public Cliente findByNome(String nome);

	public Page<Cliente> findByNomeLikeOrCpfLike(String nome, String cpf, Pageable pageable);
}
	