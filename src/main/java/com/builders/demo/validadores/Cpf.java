package com.builders.demo.validadores;

public interface Cpf {

	/**
	 * Verifica se o {@code cpf} � v�lido
	 * @return retorna verdadeiro quando o CPF for v�lido, falso caso contr�rio
	 */
	boolean isValido();

	/**
	 * Verifica se o {@code cpf} � inv�lido
	 * @return retorna verdadeiro quando o CPF n�o � v�lido, falso caso contr�rio
	 */
	boolean isInvalido();

}