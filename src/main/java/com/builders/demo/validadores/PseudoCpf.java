package com.builders.demo.validadores;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Pseuda valida��o do CPF
 * 
 * @author Alisson Gomes
 */
public class PseudoCpf implements Cpf {

	private static final Pattern PATTERN = Pattern.compile("^1?(\\d{11})");
	private String cpf;

	public PseudoCpf(String cpf) {
		this.cpf = cpf;
	}
	/**
	 * Valida se o CPF tem 11 algarismos num�ricos
	 * @return retorna verdadeiro quando o CPF tem 11 algarismos num�ricos, falso caso contr�rio
	 */
	@Override
	public boolean isValido() {
		if (Objects.isNull(cpf))
			return false;

		return PATTERN.matcher(cpf).matches();
	}

	/**
	 * Valida se o CPF n�o tem 11 algarismos num�ricos
	 * @return retorna verdadeiro quando o CPF � diferente de 11 algarismos num�ricos, falso caso contr�rio.
	 */
	@Override
	public boolean isInvalido() {
		return !isValido();
	}
}
