package com.builders.demo.validadores;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidadorUtil {
    private final static Logger LOG = LoggerFactory.getLogger(ValidadorUtil.class);

	private static Validator validator;
    static
    {
        try 
        {
            validator = Validation.buildDefaultValidatorFactory().getValidator();
        }
        catch(Exception e)
        {
            LOG.warn("Implementa��o para JSR Bean Validation n�o encontrada");
        }
    }
    
    /***
     * Executa Bean Validator para uma classe do modelo de dom�nio (JPA)
     * @param entidade do modelo de dom�nio
     * @return retorna uma mapa com as restri��es encontradas na {@code entidade}, 
     * caso nenhuma restri��o seja encontrada o mapa estar� vazio.
     */
    public static Map<String, String> validar(Object entidade)
    {
        if (validator == null)
            return Collections.emptyMap();

        Set<ConstraintViolation<Object>> violations = validator.validate(entidade);
        Map<String, String> constraints = new HashMap<String, String>(violations.size());
        for(ConstraintViolation<Object> violation : violations)
        {
            constraints.put(violation.getPropertyPath().iterator().next().getName(), violation.getMessage());
        }
        return constraints;
    }
    
    public static Cpf getCpf(String cpf) {
    	return new PseudoCpf(cpf);
    }
}
