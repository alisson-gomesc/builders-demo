package com.builders.demo.web;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.RegistroNaoEncontradoException;
import com.builders.demo.negocio.imp.CrudCliente;
import com.builders.demo.web.hateoas.ClienteModelAssembler;

@RestController
@RequestMapping("clientes")
public class ClienteController {

	private final ClienteModelAssembler assembler;

	@Autowired
	private PagedResourcesAssembler<Cliente> pagedResourcesAssembler;

	@Autowired
	private CrudCliente crudCliente;

	public ClienteController(ClienteModelAssembler assembler) {
		this.assembler = assembler;
	}

	/**
	 * Recupera os clientes sem filtros
	 * 
	 * @param pageable configura��es da p�gina a ser exibida
	 * @return retorna lista paginada dos clientes encontrados
	 */
	@GetMapping()
	public PagedModel<EntityModel<Cliente>> procurar(//
			@RequestParam(required = false) String nome, //
			@RequestParam(required = false) String cpf, //
			Pageable pageable) {
		Page<Cliente> clientes = null;
			
		if (Objects.isNull(nome) && Objects.isNull(cpf))
			clientes = this.crudCliente.procura().todos(pageable);
		else
			clientes = crudCliente.procura().comNomeOuCpf(nome, cpf, pageable);

		return pagedResourcesAssembler.toModel(clientes, assembler);
	}

	/**
	 * Recupera o cliente pela chave primaria
	 * 
	 * @param pageable configura��es da p�gina a ser exibida
	 * @return retorna lista paginada dos clientes encontrados
	 */
	@GetMapping("/{id}")
	public EntityModel<Cliente> getCliente(@PathVariable Long id) {
		Cliente cliente = crudCliente.procura().comId(id) //
				.orElseThrow(() -> new RegistroNaoEncontradoException(id));

		return assembler.toModel(cliente);
	}

	@PostMapping()
	public EntityModel<Cliente> inserir(@RequestBody Cliente clienteNovo) {

		Cliente cliente = crudCliente.inserir().executar(clienteNovo);

		return assembler.toModel(cliente);
	}

	@PutMapping()
	public EntityModel<Cliente> atualizar(@RequestBody Cliente cliente) {

		Cliente clienteAtualizado = crudCliente.atualizar().executar(cliente);

		return assembler.toModel(clienteAtualizado);
	}

	@PatchMapping("/cpf")
	public EntityModel<Cliente> atualizarCpf(@RequestBody Cliente cliente) {

		Cliente clienteAtualizado = crudCliente.atualizarCpf().executar(cliente);

		return assembler.toModel(clienteAtualizado);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id) {

		crudCliente.excluir().executar(id);

		return ResponseEntity.ok().build();
	}

}