package com.builders.demo.web.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.builders.demo.negocio.CpfException;

@ControllerAdvice
class CpfInvalidoAdvice {

	@ResponseBody
	@ExceptionHandler(CpfException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String registroNaoEncontradoHandler(CpfException ex) {
		return ex.getMessage();
	}
}
