package com.builders.demo.web.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.builders.demo.negocio.RegistroNaoEncontradoException;

@ControllerAdvice
class RegistroNaoEncontradoAdvice {

	@ResponseBody
	@ExceptionHandler(RegistroNaoEncontradoException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String registroNaoEncontradoHandler(RegistroNaoEncontradoException ex) {
		return ex.getMessage();
	}
}
