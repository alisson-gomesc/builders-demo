package com.builders.demo.web.hateoas;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.web.ClienteController;

@Component
public class ClienteModelAssembler implements RepresentationModelAssembler<Cliente, EntityModel<Cliente>> {

  @Override
  public EntityModel<Cliente> toModel(Cliente cliente) {

    return EntityModel.of(cliente, //
        linkTo(methodOn(ClienteController.class).getCliente(cliente.getId())).withSelfRel(),
        linkTo(methodOn(ClienteController.class).procurar( //
        	 	cliente.getNome(), //
        		cliente.getCpf(), //
        		PageRequest.of(0, 5))).withRel("procurar"));
  }
}