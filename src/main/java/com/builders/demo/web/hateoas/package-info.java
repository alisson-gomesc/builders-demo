/**
 * Este pacote mantém as configurações e conversões das respostas HTTP para RESTful
 * (<a href="https://en.wikipedia.org/wiki/HATEOAS">Hypermedia as the Engine of Application State</a>)
 * 
 * @author Alisson Gomes
 */
package com.builders.demo.web.hateoas;