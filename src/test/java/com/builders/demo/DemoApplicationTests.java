package com.builders.demo;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	static final Logger LOG = LoggerFactory.getLogger(DemoApplicationTests.class);
	@Test
	void contextLoads() {
		LOG.info("Aplica��o Demo iniciada e rodando com sucesso");
	}

}
