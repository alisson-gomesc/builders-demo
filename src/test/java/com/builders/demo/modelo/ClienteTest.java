package com.builders.demo.modelo;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.builders.demo.validadores.ValidadorUtil;

public class ClienteTest {

	@Test
	public void quandoCalculaIdadeNula() {
		Cliente c = new Cliente();
		assertThat(c.getIdade()).isEqualTo(0);
	}
	
	@Test
	public void quandoCalculaIdade() {
		Cliente c = new Cliente();
		
		c.setDataNascimento(LocalDate.now());
		assertThat(c.getIdade()).isEqualTo(0);

		c.setDataNascimento(LocalDate.of(2016, 2, 1));
		assertThat(c.getIdade()).isEqualTo(4);

		c.setDataNascimento(LocalDate.of(1977, 10, 20));
		assertThat(c.getIdade()).isEqualTo(43);
	}
	
	@Test
	public void quandoTodosAtributosSaoObrigatorios() {
		Cliente c = new Cliente();
		
		Map<String, String> restricoes = ValidadorUtil.validar(c);
		
		assertThat(restricoes.isEmpty()).isFalse();
		assertThat(restricoes.size()).isEqualTo(3);
	}
	
	@Test
	public void quandoNomeEhObrigatorio() {
		Cliente c = new Cliente(null, "12345678901", LocalDate.now());
		
		Map<String, String> restricoes = ValidadorUtil.validar(c);
		
		assertThat(restricoes.isEmpty()).isFalse();
		assertThat(restricoes.size()).isEqualTo(1);
		assertThat(restricoes.values().iterator().next()).isEqualTo("Nome � obrigat�rio");
	}

	@Test
	public void quandoCpfEhObrigatorio() {
		Cliente c = new Cliente("Joana", null, LocalDate.now());
		
		Map<String, String> restricoes = ValidadorUtil.validar(c);
		
		assertThat(restricoes.isEmpty()).isFalse();
		assertThat(restricoes.size()).isEqualTo(1);
		assertThat(restricoes.values().iterator().next()).isEqualTo("CPF � obrigat�rio");
	}
	
	@Test
	public void quandoDataNascimentoEhObrigatorio() {
		Cliente c = new Cliente("Joana", "12345678901", null);
		
		Map<String, String> restricoes = ValidadorUtil.validar(c);
		
		assertThat(restricoes.isEmpty()).isFalse();
		assertThat(restricoes.size()).isEqualTo(1);
		assertThat(restricoes.values().iterator().next()).isEqualTo("Data de nascimento � obrigat�rio");
	}

	@Test
	public void quandoTodosAtributosEstaoPreenchidos() {
		Cliente c = new Cliente("Joana", "12345678901", LocalDate.now());
		
		Map<String, String> restricoes = ValidadorUtil.validar(c);
		assertThat(restricoes.isEmpty()).isTrue();
	}

}
