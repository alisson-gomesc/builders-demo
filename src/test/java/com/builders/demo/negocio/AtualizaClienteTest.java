package com.builders.demo.negocio;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.imp.AtualizaClienteImp;
import com.builders.demo.negocio.imp.InsereClienteImp;
import com.builders.demo.repositorio.RepositorioCliente;

@ExtendWith(SpringExtension.class)
public class AtualizaClienteTest {

	@MockBean
    private RepositorioCliente repositorio;
	final Cliente MARIA = new Cliente("Maria", "01234567899", LocalDate.now());
	final Cliente EVA = new Cliente(15L, "Eva", "01987654321", LocalDate.now());
	
	@BeforeEach
	public void setUp() {
	    Mockito.when(repositorio.save(MARIA)).thenReturn(MARIA);
	    Mockito.when(repositorio.save(EVA)).thenReturn(EVA);
	}
	
	@Test
	public void quandoTentaAtualizarComCpfInvalido_Entao_LevantaUmaExcecao() {
		Cliente maria = new Cliente("Joana", "123", LocalDate.now());

	    assertThatThrownBy(() -> {
	    	new InsereClienteImp(this.repositorio).executar(maria);
	    }).isInstanceOf(CpfException.class)
	      .hasMessageContaining("CPF inv�lido: 123");
	 }

	@Test
	public void quandoAtualizaClienteSemChavePrimaria() {
		AtualizaCliente service = new AtualizaClienteImp(this.repositorio);
		Cliente clienteSalvo = service.executar(MARIA);
		assertThat(clienteSalvo).isNotNull();
	 }

	@Test
	public void quandoAtualizaClienteComChavePrimaria() {
		AtualizaCliente service = new AtualizaClienteImp(this.repositorio);
		Cliente clienteSalvo = service.executar(EVA);
		assertThat(clienteSalvo.getId()).isNotNull();
		assertThat(clienteSalvo.getId()).isEqualTo(15L);
	}

}
