package com.builders.demo.negocio;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.imp.AtualizaClienteImp;
import com.builders.demo.negocio.imp.AtualizaCpfClienteImp;
import com.builders.demo.negocio.imp.InsereClienteImp;
import com.builders.demo.repositorio.RepositorioCliente;

@ExtendWith(SpringExtension.class)
public class AtualizaCpfClienteTest {

	@MockBean
    private RepositorioCliente repositorio;
	final Cliente MAIOR_IDADE = new Cliente("Maria", "01234567899", LocalDate.of(1977,10,31));
	final Cliente MENOR_IDADE = new Cliente(15L, "Eva", "01987654321", LocalDate.now());
	final Cliente CLIENTE_INEXISTENTE = new Cliente(404L, "Eva", "01987654321", LocalDate.now());
	
	@BeforeEach
	public void setUp() {
	    Mockito.when(repositorio.save(MAIOR_IDADE)).thenReturn(MAIOR_IDADE);
	    Mockito.when(repositorio.save(MENOR_IDADE)).thenReturn(MENOR_IDADE);
	    Mockito.when(repositorio.findById(MENOR_IDADE.getId())).thenReturn(Optional.of(MENOR_IDADE));
	    Mockito.doThrow(new RegistroNaoEncontradoException(404L)).when(repositorio).findById(404L);
	}
	
	@Test
	public void quandoTentaAtualizarCpfMaiorIdade_Entao_LevantaUmaExcecao() {

	    assertThatThrownBy(() -> {
	    	new AtualizaCpfClienteImp(this.repositorio).executar(MAIOR_IDADE);
	    }).isInstanceOf(CpfException.class)
	      .hasMessageContaining("CPF com maioridade n�o pode ser alterado");
	 }

	@Test
	public void quandoTentaAtualizarCpfClienteInexistente_Entao_LevantaUmaExcecao() {
		
		assertThatThrownBy(() -> {
			new AtualizaCpfClienteImp(this.repositorio).executar(CLIENTE_INEXISTENTE);
		}).isInstanceOf(RegistroNaoEncontradoException.class)
		.hasMessageContaining("N�o encontrou o registro " + CLIENTE_INEXISTENTE.getId());
	}

	@Test
	public void quandoAtualizaClienteComChavePrimaria() {
		AtualizaCpfClienteImp service = new AtualizaCpfClienteImp(this.repositorio);
		Cliente clienteSalvo = service.executar(MENOR_IDADE);
		assertThat(clienteSalvo.getId()).isNotNull();
		assertThat(clienteSalvo.getId()).isEqualTo(15L);
	}

}
