package com.builders.demo.negocio;

import static org.assertj.core.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.imp.ExcluiClienteImp;
import com.builders.demo.repositorio.RepositorioCliente;

@ExtendWith(SpringExtension.class)
public class ExcluiClienteTest {

	@MockBean
    private RepositorioCliente repositorio;
	final Cliente EVA = new Cliente(15L, "Eva", "01987654321", LocalDate.now());
	
	@BeforeEach
	public void setUp() {
	    Mockito.doNothing().when(repositorio).deleteById(15L);
	    Mockito.doThrow(new IllegalArgumentException("Campo id � obrigat�rio")).when(repositorio).delete(null);
	    Mockito.doThrow(new IllegalArgumentException("Campo id � obrigat�rio")).when(repositorio).deleteById(null);
	    Mockito.doThrow(new IllegalArgumentException("Campo id � obrigat�rio")).when(repositorio).deleteById(404L);
	}
	
	@Test
	public void quandoExcluiClientePelaChavePrimaria() {
		ExcluiCliente service = new ExcluiClienteImp(this.repositorio);
		service.executar(EVA.getId());
	 }

	@Test
	public void quandoExcluiClienteComEntidade() {
		ExcluiCliente service = new ExcluiClienteImp(this.repositorio);
		service.executar(EVA);
	 }

	@Test
	public void quandoTentaExcluirEntidadeNula_Entao_LevantaUmaExcecao() {
		
	    assertThatThrownBy(() -> {
	    	Cliente clienteNulo = null;
	    	new ExcluiClienteImp(this.repositorio).executar(clienteNulo);
	    }).isInstanceOf(IllegalArgumentException.class)
	      .hasMessageContaining("Campo id � obrigat�rio");

	}

	@Test
	public void quandoTentaExcluirEntidadeComChaveInexistente_Entao_LevantaUmaExcecao() {
		
		assertThatThrownBy(() -> {
			new ExcluiClienteImp(this.repositorio).executar(404L);
		}).isInstanceOf(IllegalArgumentException.class)
		.hasMessageContaining("Campo id � obrigat�rio");

		assertThatThrownBy(() -> {
			Long id = null;
			new ExcluiClienteImp(this.repositorio).executar(id);
		}).isInstanceOf(IllegalArgumentException.class)
		.hasMessageContaining("Campo id � obrigat�rio");
	}
	
}
