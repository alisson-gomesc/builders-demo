package com.builders.demo.negocio;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.imp.InsereClienteImp;
import com.builders.demo.repositorio.RepositorioCliente;

@ExtendWith(SpringExtension.class)
public class InsereClienteTest {

	@MockBean
    private RepositorioCliente repositorio;
	
	@BeforeEach
	public void setUp() {
	    Cliente clienteNovo = new Cliente("Maria", "01234567899", LocalDate.now());
	    Cliente clienteSalvo = new Cliente("Maria", "01234567899", LocalDate.now());
	    clienteSalvo.setId(15L);
	    Mockito.when(repositorio.save(clienteNovo)).thenReturn(clienteSalvo);
	}
	
	@Test
	public void quandoTentaInserirClienteComCpfInvalido_Entao_LevantaUmaExcecao() {
		Cliente maria = new Cliente("Maria", "123", LocalDate.now());

	    assertThatThrownBy(() -> {
	    	new InsereClienteImp(this.repositorio).executar(maria);
	    }).isInstanceOf(CpfException.class)
	      .hasMessageContaining("CPF inv�lido: 123");
	 }

	@Test
	public void quandoInserirClienteValido() {
		Cliente maria = new Cliente("Maria", "01234567899", LocalDate.now());

		InsereCliente service = new InsereClienteImp(this.repositorio);
		Cliente clienteSalvo = service.executar(maria);
		assertThat(clienteSalvo.getId()).isNotNull();
		assertThat(clienteSalvo.getId()).isEqualTo(15L);
	 }

}
