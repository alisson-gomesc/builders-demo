package com.builders.demo.negocio;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.imp.ProcuraClienteImp;
import com.builders.demo.repositorio.RepositorioCliente;

@ExtendWith(SpringExtension.class)
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ProcuraClienteTest {

	@MockBean
    private RepositorioCliente repositorio;
	final Pageable PAGEABLE = PageRequest.of(0, 5);
	final Cliente EVA = new Cliente(15L, "Eva", "123", null);
	
	@BeforeEach
	public void setUp() {
	    Mockito.when(repositorio.findByNome(EVA.getNome()))
	      .thenReturn(EVA);

	    Mockito.when(repositorio.findById(EVA.getId()))
	    .thenReturn(Optional.of(EVA));

	    List<Cliente> clientes = new ArrayList<>();
	    clientes.add(EVA);
	    clientes.add(EVA);
	    clientes.add(EVA);
	    Mockito.when(repositorio.findAll(PAGEABLE))
	    .thenReturn(new PageImpl(clientes, PAGEABLE, 3));
	    
	    Mockito.when(repositorio.findByNomeLikeOrCpfLike("%Eva%", "%123%", PAGEABLE))
	    .thenReturn(new PageImpl(clientes, PAGEABLE, 3));
	}
	
	@Test
	public void quandoBuscaClientePorNomeContidoNaTabela_Entao_EleDeveSerEncontrado() {
	    String nome = "Eva";
	    Optional<Cliente> cliente = new ProcuraClienteImp(repositorio).comNome(nome);
	    assertThat(cliente.isPresent()).isTrue();
	    assertThat(cliente.get()).isNotNull();
	    assertThat(cliente.get().getNome()).isEqualTo(nome);
	 }

	@Test
	public void quandoBuscaClientePorIdContidoNaTabela_Entao_EleDeveSerEncontrado() {
	    final Long ID = 15L;
	    Optional<Cliente> cliente = new ProcuraClienteImp(repositorio).comId(ID);
	    assertThat(cliente.isPresent()).isTrue();
	    assertThat(cliente.get()).isNotNull();
	    assertThat(cliente.get().getId()).isEqualTo(ID);
	 }

	@Test
	public void quandoBuscaClientePorIdInexistente_Entao_OptionalEmptyDeveSerRetornado() {
	    final Long ID = 1500L;
	    Optional<Cliente> cliente = new ProcuraClienteImp(repositorio).comId(ID);
	    assertThat(cliente.isPresent()).isFalse();
	 }

	@Test
	public void quandoBuscaTodosClientes() {
	    Page<Cliente> clientes = new ProcuraClienteImp(repositorio).todos(PAGEABLE);
	    assertThat(clientes.isEmpty()).isFalse();
	    assertThat(clientes.getTotalElements()).isEqualTo(3);
	 }

	@Test
	public void quandoBuscaClientesPeloNomeOuCpf() {
		Page<Cliente> clientes = new ProcuraClienteImp(repositorio).comNomeOuCpf("Eva", "123", PAGEABLE);
		assertThat(clientes.isEmpty()).isFalse();
		assertThat(clientes.getTotalElements()).isEqualTo(3);
	}

}
