package com.builders.demo.repositorio;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.builders.demo.modelo.Cliente;

@SpringBootTest
public class BancoDadosTest {

	@Autowired
	private RepositorioCliente repositorio;
	
	@Test
	public void verificaSeBancoEstaRodandoComCargaInicial() {
		Page<Cliente> clientes = repositorio.findAll(PageRequest.of(0, 3));
		assertThat(clientes.getNumberOfElements()).isGreaterThan(1);
	}
	
}
