package com.builders.demo.validadores;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PseudoCpfTest {
	
	@Test
	public void quandoCpfNuloEhInvalido() {
		Cpf cpf = new PseudoCpf(null);
		assertThat(cpf.isValido()).isFalse();
	}

	@Test
	public void quandoCpfEhValido() {
		assertThat(new PseudoCpf("01234567899").isValido()).isTrue();
		assertThat(new PseudoCpf("11111111111").isValido()).isTrue();
		assertThat(new PseudoCpf("99999999999").isValido()).isTrue();
	}

	@Test
	public void quandoCpfEhValidoMasSeparadoreInvalidam() {
		assertThat(new PseudoCpf("012.345.678-99").isValido()).isFalse();
	}
	
	@Test
	public void quandoCpfTemMenosDe11Caracteres(){
		
		assertThat(new PseudoCpf("123456789").isValido()).isFalse();
	}

	@Test
	public void quandoCpfTemMaisDe11Caracteres(){
		assertThat(new PseudoCpf("012345678912").isValido()).isFalse();
	}

}
