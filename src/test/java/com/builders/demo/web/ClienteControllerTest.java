package com.builders.demo.web;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.builders.demo.modelo.Cliente;
import com.builders.demo.negocio.AtualizaCliente;
import com.builders.demo.negocio.AtualizaCpfCliente;
import com.builders.demo.negocio.ExcluiCliente;
import com.builders.demo.negocio.InsereCliente;
import com.builders.demo.negocio.ProcuraCliente;
import com.builders.demo.negocio.imp.CrudCliente;
import com.builders.demo.repositorio.RepositorioCliente;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

@WebMvcTest(ClienteController.class)
@AutoConfigureMockMvc
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ClienteControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private RepositorioCliente repositorio;
	@MockBean
	private CrudCliente crudCliente;
	@MockBean
	private ProcuraCliente procura;
	@MockBean
	private InsereCliente inserir;
	@MockBean
	private AtualizaCliente atualizar;
	@MockBean
	private AtualizaCpfCliente atualizarCpf;
	@MockBean
	private ExcluiCliente excluir;
	final Pageable PAGEABLE = PageRequest.of(0, 5);
	final Cliente EVA = new Cliente(15L, "Eva", "123", null);

	@BeforeEach
	public void setUp() {
		List<Cliente> clientes = new ArrayList<>();
		clientes.add(EVA);
		clientes.add(EVA);
		clientes.add(EVA);

		Mockito.when(crudCliente.procura()).thenReturn(procura);
		Mockito.when(crudCliente.inserir()).thenReturn(inserir);
		Mockito.when(crudCliente.atualizar()).thenReturn(atualizar);
		Mockito.when(crudCliente.atualizarCpf()).thenReturn(atualizarCpf);
		Mockito.when(crudCliente.excluir()).thenReturn(excluir);

		Mockito.when(procura.todos(Mockito.any(PageRequest.class))).thenReturn(new PageImpl(clientes, PAGEABLE, 3));

		Mockito.when(procura.comId(15L)).thenReturn(Optional.of(EVA));

		Mockito.when(procura.comNomeOuCpf(Mockito.anyString(), Mockito.anyString(), Mockito.any(PageRequest.class)))
				.thenReturn(new PageImpl(clientes, PAGEABLE, 3));

		Mockito.when(inserir.executar(Mockito.any(Cliente.class))).thenReturn(EVA);
		Mockito.when(atualizar.executar(Mockito.any(Cliente.class))).thenReturn(EVA);
		Mockito.when(atualizarCpf.executar(Mockito.any(Cliente.class))).thenReturn(EVA);

		Mockito.doNothing().when(excluir).executar(15L);
		Mockito.doThrow( //
				new EmptyResultDataAccessException(
						"No class com.builders.demo.modelo.Cliente entity with id 404 exists!", 0)) //
				.when(excluir).executar(404L);
		Mockito.doThrow( //
				new EmptyResultDataAccessException(
						"No class com.builders.demo.modelo.Cliente entity with id 404 exists!", 0)) //
		.when(repositorio).deleteById(404L);
	}

	@Test
	public void quandoProcuraTodosOsClientes() throws Exception {

		this.mockMvc.perform(get("/clientes") //
				.param("page", "0") //
				.param("size", "3")) //
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$._embedded.clienteList[0].id").value("15")) //
				.andExpect(jsonPath("$._embedded.clienteList[0].nome").value("Eva"));
	}

	@Test
	public void quandoTentaRecuperaClienteNaoExiste_Entao_LevantaNotFound() throws Exception {

		this.mockMvc.perform(get("/clientes/1")) //
				.andDo(print()) //
				.andExpect(status().isNotFound()) //
				.andExpect(content().string(containsString("N�o encontrou o registro 1")));
	}

	@Test
	public void quandoRecuperaClientePeloId() throws Exception {

		this.mockMvc.perform(get("/clientes/15")) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$.id").value("15")) //
				.andExpect(jsonPath("$.nome").value("Eva"));
	}

	@Test
	public void quandoRecuperaClientePeloNomeOuCpf() throws Exception {

		this.mockMvc.perform(get("/clientes") //
				.param("nome", "Eva") //
				.param("cpf", "123")) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$._embedded.clienteList[0].id").value("15")) //
				.andExpect(jsonPath("$._embedded.clienteList[0].nome").value("Eva"));
	}

	@Test
	public void quandoInsereNovoCliente() throws Exception {

		this.mockMvc.perform(post("/clientes") //
				.contentType(MediaType.APPLICATION_JSON) //
				.content(json(EVA))) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$.id").value("15")) //
				.andExpect(jsonPath("$.nome").value("Eva"));
	}

	@Test
	public void quandoAtualizaClienteExistente() throws Exception {

		this.mockMvc.perform(put("/clientes") //
				.contentType(MediaType.APPLICATION_JSON) //
				.content(json(EVA))) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$.id").value("15")) //
				.andExpect(jsonPath("$.nome").value("Eva"));
	}


	@Test
	public void quandoAtualizaCpfClienteExistente() throws Exception {

		this.mockMvc.perform(patch("/clientes/cpf") //
				.contentType(MediaType.APPLICATION_JSON) //
				.content(json(EVA))) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$.id").value("15")) //
				.andExpect(jsonPath("$.nome").value("Eva"));
	}
	
	@Test
	public void quandoExcluiClienteExistente() throws Exception {

		this.mockMvc.perform(delete("/clientes/15") //
				.contentType(MediaType.APPLICATION_JSON)) //
				.andDo(print()) //
				.andExpect(status().isOk()) //
				.andExpect(content().string(is("")));
	}

	@Test
	public void quandoTentaExcluirClienteInexistente() throws Exception {

		this.mockMvc.perform(delete("/clientes/404") //
				.contentType(MediaType.APPLICATION_JSON)) //
				.andDo(print()) //
				.andExpect(status().isNotFound()) //
				.andExpect(
						content().string(is("No class com.builders.demo.modelo.Cliente entity with id 404 exists!")));
	}

	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
	}

	private String json(Object instance) {
		ObjectWriter ow = JSON.writer().withDefaultPrettyPrinter();
		try {
			return ow.writeValueAsString(instance);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}